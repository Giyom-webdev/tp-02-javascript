'use strict'
const date = new Date().toLocaleDateString()
const temps = new Date().toLocaleTimeString()
const nom = { firstname: 'Guillaume', lastname: 'Gosselin' }
const dateNaissance = new Date(1990, 2, 5)
const sexe = 'M'
const situationParticuliere = 'non'
const calculAge = Date.now() - dateNaissance.getTime()
const ageDateFormat = new Date(calculAge)
const annee = ageDateFormat.getUTCFullYear()
const age = Math.abs(annee - 1970)
document.write(date + ' à ', temps + '<br>' + '<br>', nom.firstname + ' ', nom.lastname + '<br>' + '<br>', 'Âge : ' + age + ' an(s)' + '<br>' + '<br>')

let jourAnalyse = 0
fn()
function fn () {
  jourAnalyse = parseFloat(prompt('Combien de jours voulez-vous analyser?', 7))
  if (isNaN(jourAnalyse)) {
    alert('S.V.P Entrez des caracters valides')
    return fn()
  }
}
const jours = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
let i = jours[0]
let totalConso = 0
let nbBoisson = 0
let respectePasCondition = 0
let journeeNonRespectee = 0
let journeeAjeun = 0
for (i = 0; i <= jourAnalyse - 1; i++) {
  if (jourAnalyse <= 7) {
    nbBoisson = parseFloat(prompt('Combien de boisson avez vous pris ' + jours[i], 0))
    if (isNaN(nbBoisson)) {
      alert('S.V.P Entrez des caracteres valides')
      nbBoisson = parseFloat(prompt('Combien de boisson avez vous pris ' + jours[i], 0))
    }
    if (nbBoisson === null) {
      nbBoisson = 0
    }
    totalConso = totalConso + nbBoisson
    document.write('Vous avez pris ' + nbBoisson + ' consommation ' + jours[i] + '<br>')
    if (sexe === 'M' && nbBoisson > 3) {
      respectePasCondition = 1
      journeeNonRespectee = journeeNonRespectee + 1
    } else if (sexe === 'F' && nbBoisson > 2) {
      respectePasCondition = 1
      journeeNonRespectee = journeeNonRespectee + 1
    }
    if (nbBoisson === 0) {
      journeeAjeun = journeeAjeun + 1
    }
  } else {
    alert('Vous devez entrez un chiffre entre 1 et 7')
    fn()
  }
}
document.write('<br>')
const moyenne = (totalConso / jourAnalyse) * (jourAnalyse / 7)
document.write('Votre moyenne de consommation par jours est de : ' + moyenne.toFixed(2) + '<br>' + '<br>')
let recommendationHebdo = 0
if (sexe === 'M') {
  recommendationHebdo = 15
} else if (sexe === 'F') {
  recommendationHebdo = 10
} else {
  alert('Vous devez entrez une valeur M ou F')
}
const ratioHebdo = (journeeAjeun * 100) / jourAnalyse
document.write('<br>' + 'Ratio de journee sans alcool: ' + ratioHebdo.toFixed(2) + ' %')
const ratioQuoti = (journeeNonRespectee * 100) / jourAnalyse
document.write('<br>' + 'Ratio de journee excedents: ' + ratioQuoti.toFixed(2) + ' %' + '<br>' + '<br>')

if (totalConso > recommendationHebdo || respectePasCondition === 1 || situationParticuliere === 'oui') {
  document.write('Vous ne respectez pas les recommandations etablie par le TP de Philippe')
} else {
  document.write('Vous respectez la recommandation d\'alcool hebdomadaire')
}
const journeeRespectee = jourAnalyse - journeeNonRespectee
document.write('<br>' + 'Vous respectez les recommendation quotidienne ' + journeeRespectee + ' jour sur ' + jourAnalyse)
